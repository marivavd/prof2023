// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:prof2023/domain/get_onboarding_items.dart';
import 'package:prof2023/domain/queue.dart';

import 'package:prof2023/main.dart';
import 'package:prof2023/presentation/pages/onboarding.dart';

void main() {
  Queue queue = Queue();
  group('Test Onboarding', () {
    test('Изображение и текст из очереди извлекается правильно (в порядке добавления в очередь).', () {
      queue.reset_queue();
      for (var val in getOnboardingItems()){
        final currEl = queue.next();
        expect(currEl, val);
      }
    });
    test('Корректное извлечение элементов из очереди (количество элементов в очереди уменьшается на единицу).', () {
      queue.reset_queue();
      var len_queue = queue.len();
      for (var val in getOnboardingItems()){
        queue.next();
        expect(queue.len(), len_queue - 1);
        len_queue = queue.len();
      }
    });
    testWidgets('В случае, когда очередь не пуста, устанавливается правильная надпись на кнопке.', (widgetTester) async{
      await widgetTester.runAsync(() => widgetTester.pumpWidget(
        MaterialApp(
          home: OnBoarding(),
        )
      ));
      var queue = widgetTester.widget<OnBoarding>(find.byType(OnBoarding)).queue;
      while (queue.len() != 0){
        expect(find.widgetWithText(FilledButton, 'Дальше'), findsOneWidget);
        await widgetTester.tap(find.widgetWithText(FilledButton, 'Дальше'));
        await widgetTester.pumpAndSettle();
      }
    });
    testWidgets('Случай, когда очередь пуста, надпись на кнопке должна измениться на "Начать"', (widgetTester) async{
      await widgetTester.runAsync(() => widgetTester.pumpWidget(
          MaterialApp(
            home: OnBoarding(),
          )
      ));
      var queue = widgetTester.widget<OnBoarding>(find.byType(OnBoarding)).queue;
      while (queue.len() != 0){
        await widgetTester.tap(find.widgetWithText(FilledButton, 'Дальше'));
        await widgetTester.pumpAndSettle();
      }
      expect(find.widgetWithText(FilledButton, 'Начать'), findsOneWidget);
    });
    testWidgets('Если очередь пустая и пользователь нажал на кнопку “Начать”, происходит открытие экрана «SignIn» приложения.', (widgetTester) async{
      await widgetTester.runAsync(() => widgetTester.pumpWidget(
          MaterialApp(
            home: OnBoarding(),
          )
      ));
      var queue = widgetTester.widget<OnBoarding>(find.byType(OnBoarding)).queue;
      while (queue.len() != 0){
        await widgetTester.tap(find.widgetWithText(FilledButton, 'Дальше'));
        await widgetTester.pumpAndSettle();
      }
      await widgetTester.tap(find.widgetWithText(FilledButton, 'Начать'));
      await widgetTester.pumpAndSettle();
      expect(find.byKey(Key('SignIn')), findsOneWidget);
    });
  });
}
