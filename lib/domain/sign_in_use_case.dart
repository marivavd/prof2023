import 'package:prof2023/data/repository/requests.dart';
import 'package:prof2023/domain/utils.dart';

class SignInUseCase{
  Future<void> pressButton(
      String email,
      String password,
      Function(dynamic) onResponse,
      Future<void> Function(String) onError
      )async{
    requestSignIn() async{
      return await signIn(email, password);
    }
    await request(requestSignIn, onResponse, onError);
  }
}
