import 'package:prof2023/data/repository/requests.dart';
import 'package:prof2023/domain/utils.dart';

class SignOutUseCase{
  Future<void> pressButton(
      int id_user,
      Function(void) onResponse,
      Future<void> Function(String) onError
      )async{
    requestSignOut() async{
      await signOut(id_user.toString());
    }
    await request(requestSignOut, onResponse, onError);
  }
}
