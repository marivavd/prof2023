import 'package:prof2023/data/repository/requests.dart';
import 'package:prof2023/domain/utils.dart';

class SignUpUseCase{
  Future<void> pressButton(
      firstname,
      lastname,
      patronymic,
      email,
      password,
      sex,
      birth,
      confirmPass,
      Function(dynamic) onResponse,
      Future<void> Function(String) onError
      )async{
    if (password == confirmPass){
      requestSignUp() async{
       return await signUp(firstname, lastname, patronymic, email, password, sex, birth);
      }
      await request(requestSignUp, onResponse, onError);
    }
    else{
      onError('Passwords dont match');
    }

  }
}
