import 'package:dio/dio.dart';

bool checkEmail(String email){
  return RegExp(r"^[0-9a-z]+@[a-z]+\.[a-z]{2,}$").hasMatch(email);
}


Future<void> request<T>(
    Future<T> Function() func,
    Function(T) onResponse,
    Future<void> Function(String) onError
    ) async {
  try{
    var response = await func();
    onResponse(response);
  } on DioException catch (e){
    try{
      if (e.response?.data != null){
        onError(e.response.toString());
      }
      onError(e.response.toString());
    }catch(_){
      var code = e.response?.statusCode;
      if (e.response != null) {
        if (e.response!.statusCode != null){
          onError(e.toString());
        }else{
          onError(
              "STATUS: $code\n"
                  "MESSAGE: ${e.response?.statusMessage}"
          );
        }
      } else {
        onError("Ошибка при отправке запроса");
      }
    }
  }catch (e) {
    onError(e.toString());
  }
}

