import 'package:prof2023/domain/get_onboarding_items.dart';

class Queue {
  List<Map<String, String>> sp = [];

  void reset_queue() {
    sp.clear();
    sp = getOnboardingItems();
  }


  Map<String, String> next() {
    final element = sp[0];
    sp.remove(element);
    return element;
  }

  bool isEmpty() {
    return sp.isEmpty;
  }

  int len() {
    return sp.length;
  }
}







