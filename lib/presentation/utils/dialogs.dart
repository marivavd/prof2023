import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void hideLoading(BuildContext context){
  Navigator.of(context).pop();
}
void showLoading(BuildContext context){
  showDialog(context: context,
      barrierDismissible: false,
      builder: (_){
    return PopScope(
      canPop: false,
        child: Dialog(
          backgroundColor: Colors.transparent,
          surfaceTintColor: Colors.transparent,
      child: Center(
        child: CircularProgressIndicator(),
      ),
    ));
      });
}

Future<void> showError(BuildContext context, String error) async {
  await showDialog(context: context, builder: (_) => AlertDialog(
    title: const Text("Ошибка"),
    content: Text(error),
    actions: [
      TextButton(onPressed: (){Navigator.pop(context);}, child: const Text("OK"))
    ],
  ));
}

