import 'package:flutter/material.dart';
import 'package:prof2023/data/repository/shared_preference.dart';
import 'package:prof2023/domain/controllers/password_controller.dart';
import 'package:prof2023/domain/sign_in_use_case.dart';
import 'package:prof2023/domain/utils.dart';
import 'package:prof2023/presentation/pages/main.dart';
import 'package:prof2023/presentation/pages/sign_up.dart';
import 'package:prof2023/presentation/widgets/testfield.dart';
import 'package:prof2023/presentation/utils/dialogs.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignIn extends StatefulWidget {
  const SignIn({super.key});


  @override
  State<SignIn> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<SignIn> {
  SharedPreferences? sharedPreferences;


  var email = TextEditingController();
  var password = PassController();
  bool isRedEmail = false;
  bool isValid = false;
  SignInUseCase useCase = SignInUseCase();


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SharedPreferences.getInstance().then((value) async {
      sharedPreferences = value;
    });

  }


  void onChange(_){
    setState(() {
      isRedEmail = !checkEmail(email.text) && email.text.isNotEmpty;
      isValid = !isRedEmail && password.text.isNotEmpty;
    });
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: Key('SignIn'),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 18),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 30,),
              Align(
                alignment: Alignment.center,
                child: Text('Добро пожаловать!',
                style: TextStyle(
                    fontSize: 24,
                    fontFamily: 'Roboto',
                    height: 28/24,
                    fontWeight: FontWeight.w600,
                    color: Colors.black
                ),),
              ),
              SizedBox(height: 16,),
              Align(
                alignment: Alignment.center,
                child: Text('Войдите, чтобы пользоваться функциями приложения',
                  style: TextStyle(
                      fontSize: 15,
                      fontFamily: 'Roboto',
                      height: 40/30,
                      fontWeight: FontWeight.w400,
                      color: Colors.black
                  ),
                textAlign: TextAlign.center,),
              ),
              SizedBox(height: 42,),
              CustomField(label: 'Почта', controller: email, isValid: !isRedEmail, onChange: onChange),
              SizedBox(height: 16,),
              CustomField(label: 'Пароль', controller: password, onChange: onChange, enableObscure: true,),
              SizedBox(height: 54,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 56,
                  width: double.infinity,
                  child: FilledButton(
                    onPressed: (isValid)?(){
                      showLoading(context);
                      useCase.pressButton(email.text, password.text,
                          (response){
                        rememberProfile(email.text, password.text, response['user']['firstname'], response['user']['lastname'], response['user']['patronymic'], response['user']['sex'], response['user']['id'].toString(), response['user']['dateBirthDay'], sharedPreferences!);
                            hideLoading(context);
                            Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => Main(id_user: response['user']['id'],)), (Route<dynamic> route) => false,);

                          },
                          (String error)async{
                        hideLoading(context);
                        showError(context, error);
    });
                    }:null,
                    style: FilledButton.styleFrom(
                        backgroundColor: Color(0xFFFFC100),
                        disabledBackgroundColor: Color(0xFFFFC100),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)
                        )
                    ),
                    child: Text(
                      'Вход',
                      style: TextStyle(
                          fontSize: 22,
                          fontFamily: 'Jost',
                          height: 24/22,
                          fontWeight: FontWeight.w500,
                          color: Colors.white
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 16,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 56,
                  width: double.infinity,
                  child: FilledButton(
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => SignUp()));
                    },
                    style: FilledButton.styleFrom(
                        backgroundColor: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          side: BorderSide(
                            color: Color(0xFFFFC100), width: 3
                          )
                        )
                    ),
                    child: Text(
                      'Создать аккаунт',
                      style: TextStyle(
                          fontSize: 22,
                          fontFamily: 'Jost',
                          height: 24/22,
                          fontWeight: FontWeight.w500,
                          color: Color(0xFFFFC100)
                      ),
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),

      ),
    );
  }
}
