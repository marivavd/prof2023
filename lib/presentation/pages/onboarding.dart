import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:prof2023/domain/queue.dart';
import 'package:prof2023/presentation/pages/sign_in.dart';
import 'package:prof2023/presentation/utils/dialogs.dart';

class OnBoarding extends StatefulWidget {
  OnBoarding({super.key});

  Queue queue = Queue(); // создание очереди


  @override
  State<OnBoarding> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<OnBoarding> {
  PageController controller = PageController();
  Map<String, String> currentElement = {};

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.queue.reset_queue();
    currentElement = widget.queue.next();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SizedBox(
      height: double.infinity,
        width: double.infinity,
        child: PageView.builder(
            physics: NeverScrollableScrollPhysics(),
            controller: controller,
            scrollDirection: Axis.vertical,

            itemBuilder: (_, index){
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Image.asset(currentElement['icon']!),
                  Spacer(),
                  Padding(
                      padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      currentElement['text']!,
                      style: TextStyle(
                          fontSize: 22,
                          fontFamily: 'Roboto',
                          height: 155/110,
                          fontWeight: FontWeight.w500,
                          color: Color(0xFF1E1E1E)
                      ),
                    ),),
                  Spacer(),
                   Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        (widget.queue.len() == 2)?SvgPicture.asset('assets/filled.svg'):SvgPicture.asset('assets/not_filled.svg'),
                        SizedBox(width: 10,),
                        (widget.queue.len() == 1)?SvgPicture.asset('assets/filled.svg'):SvgPicture.asset('assets/not_filled.svg'),
                        SizedBox(width: 10,),
                        (widget.queue.len() == 0)?SvgPicture.asset('assets/filled.svg'):SvgPicture.asset('assets/not_filled.svg'),
                      ],
                    ),
                  Spacer(),
                  Padding(
                      padding: EdgeInsets.symmetric(horizontal: 18),
                    child: Align(
                      alignment: Alignment.center,
                      child: SizedBox(
                        height: 56,
                        width: double.infinity,
                        child: FilledButton(
                          onPressed: (){
                            (widget.queue.isEmpty())?Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => SignIn()), (Route<dynamic> route) => false,) :
                            currentElement = widget.queue.next();
                            controller.nextPage(duration: Duration(milliseconds: 3000), curve: Curves.linear);
                          },
                          style: FilledButton.styleFrom(
                            backgroundColor: Color(0xFFFFC100),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)
                            )
                          ),
                          child: Text(
                              (widget.queue.isEmpty())?'Начать':'Дальше',
                            style: TextStyle(
                                fontSize: 22,
                                fontFamily: 'Roboto',
                                height: 24/22,
                                fontWeight: FontWeight.w500,
                                color: Colors.white
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Spacer()
                ],
              );
            }),

      )
    );
  }
}
