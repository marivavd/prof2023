import 'dart:io';

import 'package:flutter/material.dart';
import 'package:prof2023/data/repository/shared_preference.dart';
import 'package:prof2023/domain/controllers/password_controller.dart';
import 'package:prof2023/domain/sign_in_use_case.dart';
import 'package:prof2023/domain/sign_out_use_case.dart';
import 'package:prof2023/domain/utils.dart';
import 'package:prof2023/presentation/pages/main.dart';
import 'package:prof2023/presentation/pages/onboarding.dart';
import 'package:prof2023/presentation/pages/sign_up.dart';
import 'package:prof2023/presentation/widgets/testfield.dart';
import 'package:prof2023/presentation/utils/dialogs.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Splash extends StatefulWidget {
  Splash({super.key});


  @override
  State<Splash> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Splash> {
  SignInUseCase useCase = SignInUseCase();
  SharedPreferences? sharedPreferences;
  List<String> initials = [];
  bool flag = false;


  @override
  void initState() {
    super.initState();
    DateTime time = DateTime.now();
    SharedPreferences.getInstance().then((value) async {
      sharedPreferences = value;
      initials = await getCheck(sharedPreferences!);
      if (initials.length != 0) {
        await useCase.pressButton(
            initials[0],
            initials[1],
                (response) {
              rememberProfile(
                  initials[0],
                  initials[1],
                  response['user']['firstname'],
                  response['user']['lastname'],
                  response['user']['patronymic'],
                  response['user']['sex'],
                  response['user']['id'].toString(),
                  response['user']['dateBirthDay'],
                  sharedPreferences!);
              if (DateTime.now().difference(time).inMilliseconds > 1000){
                Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
                    builder: (context) =>
                        Main(id_user: response['user']['id'],)), (
                    Route<dynamic> route) => false,);
              }
              else{
                Future.delayed(Duration(seconds: 1000 - DateTime.now().difference(time).inMilliseconds), (){
                  Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
                      builder: (context) =>
                          Main(id_user: response['user']['id'],)), (
                      Route<dynamic> route) => false,);
                });
              }

            },
                (String error) async {
                  if (DateTime.now().difference(time).inMilliseconds > 1000){
              Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
                  builder: (context) =>
                      OnBoarding()), (
                  Route<dynamic> route) => false,);}
                  else{
                    Future.delayed(Duration(seconds: 1000 - DateTime.now().difference(time).inMilliseconds), (){
                      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
                          builder: (context) =>
                              OnBoarding()), (
                          Route<dynamic> route) => false,);
                    });

                  }
            }
        );
      }
      else{
        if (DateTime.now().difference(time).inMilliseconds > 1000){
          Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
              builder: (context) =>
                  OnBoarding()), (
              Route<dynamic> route) => false,);}
        else{
          Future.delayed(Duration(seconds: 1000 - DateTime.now().difference(time).inMilliseconds), (){
            Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
                builder: (context) =>
                    OnBoarding()), (
                Route<dynamic> route) => false,);
          });

        }
      }
    }
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFFFFD03F),
        body: Center(
            child: Text(
              'codemania',
              style: TextStyle(
                  fontSize: 42,
                  fontFamily: 'Jost',
                  height: 24/42,
                  fontWeight: FontWeight.w500,
                  letterSpacing: 15,
                  color: Colors.white,
              ),
            ),
        )
    );
  }
}
