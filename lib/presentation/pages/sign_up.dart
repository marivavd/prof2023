import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:prof2023/domain/sign_in_use_case.dart';
import 'package:prof2023/domain/sign_up_use_case.dart';
import 'package:prof2023/domain/utils.dart';
import 'package:prof2023/presentation/pages/main.dart';
import 'package:prof2023/presentation/pages/sign_in.dart';
import 'package:prof2023/presentation/widgets/testfield.dart';
import 'package:prof2023/presentation/utils/dialogs.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});


  @override
  State<SignUp> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<SignUp> {
  var email = TextEditingController();
  var password = TextEditingController();
  var surname = TextEditingController();
  var confirm_password = TextEditingController();
  var name = TextEditingController();
  var pantronymic = TextEditingController();
  var birth = TextEditingController();
  String date = '';
  bool isRedEmail = false;
  bool isValid = false;
  SignUpUseCase useCase = SignUpUseCase();
  List<DropdownMenuItem<String>> menuItems = [
      DropdownMenuItem(child: Text("Мужчина"),value: "Мужчина"),
      DropdownMenuItem(child: Text("Женщина"),value: "Женщина"),
    ];
  String sex = 'Мужчина';


  void onChange(_){
    setState(() {
      isRedEmail = !checkEmail(email.text) && email.text.isNotEmpty;
      isValid = !isRedEmail && password.text.isNotEmpty && confirm_password.text.isNotEmpty && surname.text.isNotEmpty && name.text.isNotEmpty && pantronymic.text.isNotEmpty && birth.text.isNotEmpty;
    });
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 18),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 30,),
              Align(
                alignment: Alignment.center,
                child: Text('Добро пожаловать!',
                  style: TextStyle(
                      fontSize: 24,
                      fontFamily: 'Roboto',
                      height: 28/24,
                      fontWeight: FontWeight.w600,
                      color: Colors.black
                  ),),
              ),
              SizedBox(height: 16,),
              Align(
                alignment: Alignment.center,
                child: Text('Создайте пользователя, чтобы пользоваться функциями приложения',
                  style: TextStyle(
                      fontSize: 15,
                      fontFamily: 'Roboto',
                      height: 40/30,
                      fontWeight: FontWeight.w400,
                      color: Colors.black
                  ),
                  textAlign: TextAlign.center,),
              ),
              SizedBox(height: 7,),
              CustomField(label: 'Фамилия', controller: surname, onChange: onChange,),
              SizedBox(height: 16,),
              CustomField(label: 'Имя', controller: name, onChange: onChange,),
              SizedBox(height: 16,),
              CustomField(label: 'Отчество', controller: pantronymic, onChange: onChange,),
              SizedBox(height: 16,),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                 'Пол',
                  style: TextStyle(
                      fontSize: 14,
                      height: 20/14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xFF263238),
                      fontFamily: 'Roboto'
                  )
              ),
              SizedBox(height: 4,),
              Container(
                height: 48,
                decoration: BoxDecoration(
                    color: Color(0xFFE6E6E6),
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: Colors.transparent)
                ),

                child: DropdownButtonFormField(
                  items: menuItems,
                  value: sex,
                  decoration: InputDecoration(
                      enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.transparent)),
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent)),
                    contentPadding: const EdgeInsets.symmetric(vertical: 14, horizontal: 10),
                  ),

                  dropdownColor:Color(0xFFE6E6E6),
                  borderRadius: BorderRadius.circular(10),
                  style: TextStyle(
                      fontSize: 16,
                      height: 20/16,
                      fontWeight: FontWeight.w400,
                      color: Color(0xFF263238),
                      fontFamily: 'Roboto',

                  ),

                  onChanged: (String? newValue){
                    setState(() {
                      sex = newValue!;
                      onChange;
                    });
                  },),
              )

            ],
          ),

              SizedBox(height: 16,),
              Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                  'Дата рождения',
                  style: TextStyle(
                      fontSize: 14,
                      height: 20/14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xFF263238),
                      fontFamily: 'Roboto'
                  )
              ),
              SizedBox(height: 4,),
              Container(
                height: 48,
                decoration: BoxDecoration(
                    color: Color(0xFFE6E6E6),
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: Colors.transparent)
                ),
                child: TextField(
                  controller: birth,
                  style: TextStyle(
                      fontSize: 16,
                      height: 20/16,
                      fontWeight: FontWeight.w400,
                      color: Color(0xFF263238),
                      fontFamily: 'Roboto'
                  ),
                  onChanged: onChange,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: const EdgeInsets.symmetric(vertical: 14, horizontal: 10),

                  ),
                  onTap: ()async{
                    DateTime? pickeddate = await showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate:DateTime(1900),
                        lastDate: DateTime(2100));
                    if (pickeddate != null){
                      setState(() {
                        date = DateFormat('dd.MM.yyyy').format(pickeddate);
                        initializeDateFormatting('ru_Ru').then((_) {
                          birth.text = DateFormat('dd MMMM yyyy', 'ru_Ru').format(pickeddate);
                        });


                      });
                    }
                  },
                ),
              )

            ],
          ),
              SizedBox(height: 16,),
              CustomField(label: 'Почта', controller: email, isValid: !isRedEmail, onChange: onChange,),
              SizedBox(height: 16,),
              CustomField(label: 'Пароль', controller: password, onChange: onChange, enableObscure: true,),
              SizedBox(height: 16,),
              CustomField(label: 'Повторите пароль', controller: confirm_password, onChange: onChange, enableObscure: true,),
              SizedBox(height: 54,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 56,
                  width: double.infinity,
                  child: FilledButton(
                    onPressed: (isValid)?(){
                      showLoading(context);
                      useCase.pressButton(name.text,
                          surname.text,
                          pantronymic.text,
                          email.text,
                          password.text,
                          sex,
                          date,
                          confirm_password.text,
                              (response){
                                hideLoading(context);
                                Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => Main(id_user: response['user']['id'],)), (Route<dynamic> route) => false,);

                              },

                              (String error)async{
                        hideLoading(context);
                        showError(context, error);});
                    }:null,
                    style: FilledButton.styleFrom(
                        backgroundColor: Color(0xFFFFC100),
                        disabledBackgroundColor: Color(0xFFFFC100),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)
                        )
                    ),
                    child: Text(
                      'Регистрация',
                      style: TextStyle(
                          fontSize: 22,
                          fontFamily: 'Jost',
                          height: 24/22,
                          fontWeight: FontWeight.w500,
                          color: Colors.white
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 16,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 56,
                  width: double.infinity,
                  child: FilledButton(
                    onPressed: (){
                     Navigator.of(context).pop();
                    },
                    style: FilledButton.styleFrom(
                        backgroundColor: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                            side: BorderSide(
                                color: Color(0xFFFFC100), width: 3
                            )
                        )
                    ),
                    child: Text(
                      'Назад',
                      style: TextStyle(
                          fontSize: 22,
                          fontFamily: 'Jost',
                          height: 24/22,
                          fontWeight: FontWeight.w500,
                          color: Color(0xFFFFC100)
                      ),
                    ),
                  ),
                ),
              ),

            ],
          ),
        ),

      ),
    );
  }
}
