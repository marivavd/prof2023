import 'dart:io';

import 'package:flutter/material.dart';
import 'package:prof2023/domain/controllers/password_controller.dart';
import 'package:prof2023/domain/sign_in_use_case.dart';
import 'package:prof2023/domain/sign_out_use_case.dart';
import 'package:prof2023/domain/utils.dart';
import 'package:prof2023/presentation/pages/sign_up.dart';
import 'package:prof2023/presentation/widgets/testfield.dart';
import 'package:prof2023/presentation/utils/dialogs.dart';

class Main extends StatefulWidget {
  Main({super.key, required this.id_user});
  int id_user;


  @override
  State<Main> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Main> {
  SignOutUseCase useCase = SignOutUseCase();



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 18),
          child:SizedBox(
            height: 56,
            width: double.infinity,
            child: FilledButton(
              onPressed: ()async{
                showLoading(context);
                useCase.pressButton(widget.id_user,
                    (_){
                      exit(0);
                    },
                    (String e)async{
                  hideLoading(context);
                  await showError(context, e);
                    });
              },
              style: FilledButton.styleFrom(
                  backgroundColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                      side: BorderSide(
                          color: Color(0xFFFFC100), width: 3
                      )
                  )
              ),
              child: Text(
                'Выход',
                style: TextStyle(
                    fontSize: 22,
                    fontFamily: 'Jost',
                    height: 24/22,
                    fontWeight: FontWeight.w500,
                    color: Color(0xFFFFC100)
                ),
              ),
            ),
          ),
        )
      )
    );
  }
}
