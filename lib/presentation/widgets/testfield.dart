import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CustomField extends StatefulWidget{
  final String label;
  final TextEditingController controller;
  final bool enableObscure;
  final bool isValid;
  final Function(String)? onChange;

  const CustomField({super.key, required this.label, required this.controller, this.enableObscure = false, this.isValid = true, this.onChange});


  @override
  State<CustomField> createState() => _CustomFieldState();
}

class _CustomFieldState extends State<CustomField> {
  bool isObscure = true;


  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.label,
          style: TextStyle(
            fontSize: 14,
            height: 20/14,
            fontWeight: FontWeight.w400,
            color: Color(0xFF263238),
            fontFamily: 'Roboto'
          )
        ),
        SizedBox(height: 4,),
        Container(
          height: 48,
          decoration: BoxDecoration(
            color: Color(0xFFE6E6E6),
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: Colors.transparent)
          ),
          child: TextField(
            controller: widget.controller,
            obscuringCharacter: '*',
            obscureText: (widget.enableObscure) ? isObscure:false,
            style: TextStyle(
                fontSize: 16,
                height: 20/16,
                fontWeight: FontWeight.w400,
                color: Color(0xFF263238),
                fontFamily: 'Roboto'
            ),
            onChanged: widget.onChange,
            decoration: InputDecoration(
                enabledBorder: (widget.isValid) ? Theme.of(context).inputDecorationTheme.enabledBorder : Theme.of(context).inputDecorationTheme.errorBorder,
                focusedBorder: (widget.isValid) ? Theme.of(context).inputDecorationTheme.focusedBorder : Theme.of(context).inputDecorationTheme.errorBorder,
                border: InputBorder.none,
                contentPadding: const EdgeInsets.symmetric(vertical: 14, horizontal: 10),
                suffixIconConstraints: const BoxConstraints(minWidth: 34),
                suffixIcon: (widget.enableObscure)?
                GestureDetector(
                  onTap: (){
                    setState(() {
                      isObscure = !isObscure;
                    });
                  },
                  child: SvgPicture.asset(
                    (!isObscure)
                        ? "assets/eye.svg"
                        : 'assets/eye-slash.svg',
                    //colorFilter: ColorFilter.mode(colors.x141414, BlendMode.color)
                  ),
                ):null
            ),
          ),
        )

      ],
    );
  }


}
