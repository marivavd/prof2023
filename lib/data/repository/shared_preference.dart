import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<List<String>> getCheck(SharedPreferences sharedPreferences) async {
  return sharedPreferences.getStringList("items") ?? [];
}


void rememberProfile(String email, String password, firstname,
    lastname,
    patronymic,
    sex,
    id,
    birth,SharedPreferences sharedPreferences) async {
  setCounter(email, password, firstname,
      lastname,
      patronymic,
      sex,
      id,
      birth,sharedPreferences);
}
Future<void> setCounter(
    String email,
    String password,
    firstname,
    lastname,
    patronymic,
    sex,
    id,
    birth,SharedPreferences sharedPreferences) async {
  sharedPreferences.setStringList("items", <String>[email, password, id, firstname, lastname, patronymic, sex, birth]);
}