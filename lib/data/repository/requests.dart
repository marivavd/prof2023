import 'dart:convert';

import 'package:dio/dio.dart';

final dio = Dio();

Future<Map<String, dynamic>> signIn(email, password) async {
  var formData =
    {
      "email": email,
      "password": password
    };

  Response response = await dio.post(
      "http://10.0.2.2:8080/signIn",

    data: formData

  );
  Map<String, dynamic> data = response.data;

  return data;
}


Future<Map<String, dynamic>> signUp(firstname, lastname, patronymic, email, password, sex, birth) async {
  var formData =
  {
    "firstname": firstname,
    "lastname": lastname,
    "patronymic": patronymic,
    "password": password,
    "sex": sex,
    "email": email,
    "dateBirthDay": birth
  };

  Response response = await dio.post(
      "http://10.0.2.2:8080/signUp",

      data: formData

  );
  Map<String, dynamic> data = response.data;

  return data;
}
Future<void> signOut(String id_user) async {

  Response response = await dio.post(
      "http://10.0.2.2:8080/signOut",

      options: Options(
        headers: {'idUser': id_user}
      )

  );
}
